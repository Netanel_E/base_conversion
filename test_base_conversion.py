import pytest
from base_conversion import *


def test_is_valid_return_true():
    assert is_valid("DeadBeef", "16") is True


def test_is_valid_return_false():
    assert is_valid("DeadBeef", "15") is False


def test_list_chars_of_a_base_16():
    assert list_chars_of_a_base(16) == ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']


def test_list_chars_of_a_base_2():
    assert list_chars_of_a_base(2) == ['0', '1']


def test_list_chars_of_a_base_1():
    assert list_chars_of_a_base(1) is None


def test_base_conversion_16_2():
    assert base_conversion("DeadBeef", 16, 2) == "11011110101011011011111011101111"


def test_base_conversion_14_2():
    assert base_conversion("DeadBeef", 14, 2) is None


def test_base_conversion_16_10():
    assert base_conversion("DeadBeef", 16, 10) == "3735928559"


def test_base_conversion_36_16():
    assert base_conversion("1PS9wXB", 36, 16) == "DEADBEEF"

