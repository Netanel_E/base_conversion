FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


def is_valid(num_as_string, base):
    """
    This function validate if number in string is valid in a given base.
    :param num_as_string: get number in string representation
    :param base: get a basis as int
    :return: True if 'num_as_string' represent a valid number in basis 'base', and False otherwise.
    """
    base_chars = list_chars_of_a_base(base)
    if not base_chars:
        return False

    num_as_string = num_as_string.upper()
    for i in range(len(num_as_string)):
        # check if num_as_string is valid
        if num_as_string[i] not in base_chars:
            return False
    return True


def base_input_is_valid(base_name):
    src = input("please enter {} base: ".format(base_name))
    if not src.isdigit() or not (2 <= int(src) <= 36):
        print("{} base {} is not a legal base".format(base_name, src))
        return base_input_is_valid(base_name)
    return int(src)


def src_to_decimal(num_as_string, src_base, value_to_char_dict):
    # convert from source base to decimal base
    values_list = []
    decimal_value = 0
    for char in num_as_string:
        values_list.append(value_to_char_dict[char])
    for i in range(len(values_list)):
        decimal_value += (values_list[-i - 1] * (src_base ** i))
    return decimal_value


def decimal_to_dst(decimal_value, dst_base):
    dst_values = []
    while 0 != decimal_value:
        dst_values.append(decimal_value % dst_base)
        decimal_value = int(decimal_value / dst_base)
    return dst_values


def list_chars_of_a_base(base):
    """
    This function get a base and return list of the chars in that base.
    :param base: get a basis as int
    :return: list of the chars in this base
    """
    try:
        base = int(base)
        if not (2 <= base <= 36):
            print("Source base {} is not a legal base".format(base))
            return None
        elif base <= 10:
            char_list = list(range(ord(FIRST_DIGIT), ord(FIRST_DIGIT) + base, 1))
            return list(map(chr, char_list))
        else:
            char_list = list(range(ord(FIRST_DIGIT), ord(LAST_DIGIT) + 1, 1))
            char_list += list(range(ord(FIRST_CHARACTER), ord(FIRST_CHARACTER) + (base - 10), 1))
            return list(map(chr, char_list))

    except ValueError:
        print("ValueError")
    except SyntaxError:
        print("SyntaxError")


def base_conversion(num_as_string, src_base, dst_base):
    """
    This function gets number as string, and convert number from src base to dst base representation
    :param num_as_string: get number in string representation
    :param src_base: get source base to convert from
    :param dst_base: get destination base to convert to
    :return: converted number as string in new base representation
    """
    # create a dictionary for all bases values
    value_to_char_dict = {}
    char_to_value_dict = {}
    chars_list = list_chars_of_a_base(36)
    for i in range(len(chars_list)):
        value_to_char_dict[chars_list[i]] = i
        char_to_value_dict[i] = chars_list[i]

    # check if num_as_string is valid to convert to src_base using the function is_valid
    if is_valid(num_as_string, src_base):
        print("The number {} is a legal number in base {}".format(num_as_string, src_base))
    else:
        print("The number {} is not a legal number in base {}".format(num_as_string, src_base))
        return None
    # convert chars from lowercase to uppercase.
    num_as_string = num_as_string.upper()

    # convert from source base to decimal base
    decimal_value = src_to_decimal(num_as_string, src_base, value_to_char_dict)

    # convert from decimal base to destination base
    dst_values = decimal_to_dst(decimal_value, dst_base)

    dst_num_as_string = []
    for i in range(len(dst_values)):
        dst_num_as_string.append(char_to_value_dict[dst_values[-i - 1]])
    dst_num_as_string = "".join(dst_num_as_string)
    print("The number {} in base {} is {}".format(num_as_string, src_base, dst_num_as_string))
    return dst_num_as_string


def main():
    num_as_string = input("please enter number as string: ")
    src_base = base_input_is_valid("source")
    dst_base = base_input_is_valid("destination")

    base_conversion(num_as_string, src_base, dst_base)


if '__main__' == __name__:
    main()
